Note:
> Default Device Name Filter is "PS"   

# BlueTooth Heart Rate Band Monitor App(v6.1.0b)
This App is example to read BLE standard Heart Rate  by using InMethod Android BlueTooth Framework.    
https://bitbucket.org/inmethod/bluetoothframework    

## APP Play Store
https://play.google.com/store/apps/details?id=inmethod.bluetoothframework.example.miband1s

## System Requirement 
* Android 6.0 or above
* Android wear 6.0 or above

## Develop Environment
* Android Studio 3.2 or above

## BlueTooth Heart Rate Measurement ( 0x2A37 )
https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.heart_rate_measurement.xml