package inmethod.bluetoothframework.example.miband1s;

import android.bluetooth.BluetoothAdapter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

import ble.std.protocol.BTCommandsHandler.BTCommandsHandler;
import ble.std.protocol.HearRate.*;
import ble.std.protocol.battery.BTCommandsBatteryLevel;
import inmethod.android.bt.BTInfo;
import inmethod.android.bt.DeviceConnection;
import inmethod.android.bt.command.BTCommands;
import inmethod.android.bt.handler.CommandCallbackHandler;
import inmethod.android.bt.handler.ConnectionCallbackHandler;
import inmethod.android.bt.handler.DiscoveryServiceCallbackHandler;
import inmethod.android.bt.interfaces.IChatService;
import inmethod.android.bt.interfaces.IDiscoveryService;
import inmethod.android.bt.le.LeChatService;
import inmethod.android.bt.le.LeDiscoveryService;

import android.os.*;
import android.Manifest;
import android.app.*;
import android.content.pm.*;
import android.content.*;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;

public class WearHRActivity extends WearableActivity implements DataApi.DataListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "BleBand";
    private static final SimpleDateFormat AMBIENT_DATE_FORMAT = new SimpleDateFormat("HH:mm", Locale.US);

    private BoxInsetLayout mContainerView;
    private TextView mClockView;
    private IDiscoveryService aIBlueToothDiscoveryService = null;
    private DeviceConnection aBlueToothDeviceConnection = null;

    private IChatService aIBlueToothChatService = null;
    public static final String HeartRateUUID = "00002a37-0000-1000-8000-00805f9b34fb";
    private int MAX_HEART_RATE;
    public boolean bStopForSettings = false;
    private boolean bSkip = false;
    private int iPreviousHR = 0;
    private int iCurrentHR = 0;
    private TextView aTvHRLog = null;
    private boolean bVibrated70 = false;
    private boolean bVibrated80 = false;
    private boolean bVibrated90 = false;
    private WearableActivity activity;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private PowerManager.WakeLock wakeLock = null;
    private GoogleApiClient mGoogleApiClient;
    private static int iAGE;
    private static int iTolerance;
    private static String sDeviceNameFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setAmbientEnabled();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
        }
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");

        mGoogleApiClient.connect();
        if (wakeLock != null && !wakeLock.isHeld())
            wakeLock.acquire();

        activity = this;


        iAGE = PreferenceManager.getDefaultSharedPreferences(activity).getInt("AGE", 35);
        iTolerance = PreferenceManager.getDefaultSharedPreferences(activity).getInt("ToLerance", 20);
        sDeviceNameFilter = PreferenceManager.getDefaultSharedPreferences(activity).getString("DeviceName", "PS,350,810,850");

        bVibrated70 = false;
        bVibrated80 = false;
        bVibrated90 = false;
        mContainerView = (BoxInsetLayout) findViewById(R.id.container);
        mContainerView.getBackground().setAlpha(50);
        aTvHRLog = (TextView) findViewById(R.id.HRLog);
        mClockView = (TextView) findViewById(R.id.clock);
        aTvHRLog.setMovementMethod(ScrollingMovementMethod.getInstance());

        MAX_HEART_RATE = 220 - iAGE;
        bStopForSettings = false;
        boolean bSkip = false;
        iPreviousHR = 0;
        iCurrentHR = 0;
        // Use BLE discovery Service
        aIBlueToothDiscoveryService = LeDiscoveryService.getInstance();
        // Set context()
        aIBlueToothDiscoveryService.setContext(activity);
        ///  Set device name filter
        Vector<String> aBlueToothDeviceNameFilter = new Vector<String>();

        StringTokenizer aST = new StringTokenizer(sDeviceNameFilter, ",");
        while (aST.hasMoreTokens()) {
            aBlueToothDeviceNameFilter.add(aST.nextToken());
        }

        aIBlueToothDiscoveryService.setBlueToothDeviceNameFilter(aBlueToothDeviceNameFilter);
        // Set CallBack handler
        aIBlueToothDiscoveryService.setCallBackHandler(new MyBlueToothDiscoveryServiceCallbackHandler());
        try {
            aIBlueToothDiscoveryService.startService();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {

                if (grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }


    @Override
    public void onDestroy() {
        if (aBlueToothDeviceConnection != null && aBlueToothDeviceConnection.isConnected()) {
            aBlueToothDeviceConnection.stop();
            aBlueToothDeviceConnection = null;
        }
        if (aIBlueToothDiscoveryService != null && aIBlueToothDiscoveryService.isRunning()) {
            aIBlueToothDiscoveryService.cancelDiscovery();
            aIBlueToothDiscoveryService.stopService();
            aIBlueToothDiscoveryService = null;
        }
        if (wakeLock != null && wakeLock.isHeld()) wakeLock.release();
        Toast.makeText(activity, "Disconnect device!", Toast.LENGTH_SHORT).show();

        super.onDestroy();
    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
        updateDisplay();
    }

    @Override
    public void onUpdateAmbient() {
        super.onUpdateAmbient();
        updateDisplay();
    }

    @Override
    public void onExitAmbient() {
        updateDisplay();
        super.onExitAmbient();
    }

    private void updateDisplay() {
        if (isAmbient()) {
            mContainerView.setBackgroundColor(getResources().getColor(android.R.color.black));
            mClockView.setVisibility(View.VISIBLE);
            aTvHRLog.setTextColor(Color.WHITE);
            mClockView.setText(AMBIENT_DATE_FORMAT.format(new Date()));
        } else {
            aTvHRLog.setTextColor(Color.BLUE);
            mContainerView.setBackground(null);
            mClockView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onConnected(Bundle bundle) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Connected to Google Api Service");
        }
        Wearable.DataApi.addListener(mGoogleApiClient, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        for (DataEvent event : dataEventBuffer) {
            if (event.getType() == DataEvent.TYPE_DELETED) {
                Log.d(TAG, "DataItem deleted: " + event.getDataItem().getUri());
            } else if (event.getType() == DataEvent.TYPE_CHANGED) {
                Log.d(TAG, "DataItem changed: " + event.getDataItem().getUri());
            }
        }

        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEventBuffer);
        for (DataEvent event : events) {
            final Uri uri = event.getDataItem().getUri();
            final String path = uri != null ? uri.getPath() : null;
            if ("/HR_CONFIG".equals(path)) {
                final DataMap map = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
                // read your values from map:
                iAGE = map.getInt("AGE");
                PreferenceManager.getDefaultSharedPreferences(activity).edit().putInt("AGE", iTolerance).commit();

                iTolerance = map.getInt("TOLERANCE");
                PreferenceManager.getDefaultSharedPreferences(activity).edit().putInt("ToLerance", iTolerance).commit();

                sDeviceNameFilter = map.getString("DEVICE_NAME");
                PreferenceManager.getDefaultSharedPreferences(activity).edit().putString("DeviceName", sDeviceNameFilter).commit();

                MAX_HEART_RATE = 220 - iAGE;

                Toast.makeText(activity, "device name filter changed to  = " + sDeviceNameFilter, Toast.LENGTH_SHORT).show();

                if (aBlueToothDeviceConnection != null && aBlueToothDeviceConnection.isConnected()) {
                    aBlueToothDeviceConnection.stop();
                }
                aBlueToothDeviceConnection = null;
                if (aIBlueToothDiscoveryService != null && aIBlueToothDiscoveryService.isRunning()) {
                    Vector<String> aBlueToothDeviceNameFilter = new Vector<String>();
                    aBlueToothDeviceNameFilter.add(sDeviceNameFilter);
                    aIBlueToothDiscoveryService.setBlueToothDeviceNameFilter(aBlueToothDeviceNameFilter);
                    aIBlueToothDiscoveryService.doDiscovery();
                }
//                Toast.makeText(activity, "device name from phone = "+sDeviceNameFilter, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onStop() {
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            Wearable.DataApi.removeListener(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    public class MyBlueToothDiscoveryServiceCallbackHandler extends DiscoveryServiceCallbackHandler {


        @Override
        public void StartServiceStatus(boolean bStatus, int iCode) {
            if (bStatus && iCode == DiscoveryServiceCallbackHandler.START_SERVICE_SUCCESS) {
                if (aIBlueToothDiscoveryService.isRunning())
                    aIBlueToothDiscoveryService.doDiscovery();
            } else if (!bStatus && iCode == DiscoveryServiceCallbackHandler.START_SERVICE_BLUETOOTH_NOT_ENABLE) {
                Toast.makeText(activity, "SERVICE_BLUETOOTH_NOT_ENABLE ", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void DeviceDiscoveryStatus(boolean bStatus, BTInfo aBTInfo) {
            if (bStatus) {
                // enable notification
                ArrayList<String> aNotifyUUIDs = new ArrayList<String>();
                aNotifyUUIDs.add(BTCommandsBatteryLevel.BATTERY_LEVEL_UUID);
                aNotifyUUIDs.add(HeartRateUUID);
                aIBlueToothChatService = new LeChatService(BluetoothAdapter.getDefaultAdapter(), activity, aNotifyUUIDs);
                // create new connection object and set up call back handler
                aBlueToothDeviceConnection = new DeviceConnection(aBTInfo, activity,
                        aIBlueToothChatService, new MyBlueToothConnectionCallbackHandler());
                // Connect to device
                aBlueToothDeviceConnection.connect();
                Toast.makeText(activity, "device=" + aBTInfo.getDeviceName() + ",address=" + aBTInfo.getDeviceAddress(), Toast.LENGTH_SHORT).show();
            } else if (!bStatus) {
                Log.d(TAG, "Device not found!");
                try {
                    aIBlueToothDiscoveryService.doDiscovery();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class MyBlueToothConnectionCallbackHandler extends ConnectionCallbackHandler {

        @Override
        public void DeviceConnectionStatus(boolean bStatus,BTInfo aBTInfo) {
            if (bStatus) {

            }else{
                if (aBlueToothDeviceConnection != null && !bStopForSettings)
                    aBlueToothDeviceConnection.connect();
            }
        }


        @Override
        public void NotificationStatus(boolean bStatus,BTInfo aBTInfo,String sReaderUUID) {
            Log.d(TAG, "status=" + bStatus);
            if (bStatus) {
                sendBTCommands();
            }
            else{
                Log.d(TAG, "NotificationEnableFail");
            }
        }
    }

    private void sendBTCommands() {


        BTCommands aBTCommands = new BTCommandsHRMeasurement(iAGE);
        aBTCommands.setTimeout(0); // no timeout
        aBTCommands.setCallBackHandler(
                new BTCommandsHandler() {
                    @Override
                    public void BTCommandsHeartRateMeasurementSuccess(HeartRateValue aHeartRateValue, BTInfo aInfo) {

                        if (aHeartRateValue == null) return;
                        iCurrentHR = aHeartRateValue.getHeartRate();
                        if (iCurrentHR > 220 || iCurrentHR < 30) return;

                        if (iPreviousHR == 0) iPreviousHR = iCurrentHR;
                        Log.d(TAG, "Current HeartRate=" + iCurrentHR + ",Previous =" + iPreviousHR);
                        if ((iCurrentHR > (iPreviousHR + iTolerance)) || (iCurrentHR < (iPreviousHR - iTolerance))) {
                            if (bSkip) {
                                bSkip = false;
                                iPreviousHR = iCurrentHR;
                            } else bSkip = true;
                        } else {
                            iPreviousHR = iCurrentHR;
                            bSkip = false;
                        }
                        BTLog(iCurrentHR);
                        if (!bSkip) {
                            if (iCurrentHR >= ((int) MAX_HEART_RATE * 0.7)) {
                                if (iCurrentHR >= ((int) MAX_HEART_RATE * 0.9)) {
                                    if (!bVibrated90) {
                                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                        v.vibrate(2000);
                                        bVibrated90 = true;
                                    }
                                    bVibrated80 = false;
                                    bVibrated70 = false;
                                } else if (iCurrentHR >= ((int) MAX_HEART_RATE * 0.8)) {
                                    if (!bVibrated80) {
                                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                        v.vibrate(1000);
                                        bVibrated80 = true;
                                    }

                                    bVibrated90 = false;
                                    bVibrated70 = false;
                                } else if (iCurrentHR >= ((int) MAX_HEART_RATE * 0.7)) {
                                    if (!bVibrated70) {
                                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                        v.vibrate(400);
                                        bVibrated70 = true;
                                    }
                                    bVibrated90 = false;
                                    bVibrated80 = false;
                                }
                            } else {
                            }
                        } else {
                            Log.d(TAG, "Heart Rate Not Reasonable current = " + iCurrentHR + ",Previous = " + iPreviousHR);
                        }

                    }

                    @Override
                    public void BTCommandsHeartRateMeasurementTimeout(BTInfo aInfo) {
                        Log.d(TAG, "command Timeout");
                    }
                }
        );
        aBlueToothDeviceConnection.sendBTCommands(aBTCommands);

    }

    private void BTLog(final int iHR) {

        runOnUiThread(new Runnable() {
                          public void run() {
                              DecimalFormat df = new DecimalFormat("###");
                              String sHR = df.format(iHR);
                              sHR = String.format("%3s", sHR);
                              aTvHRLog.setText(sHR);
                          }
                      }
        );

    }


}
