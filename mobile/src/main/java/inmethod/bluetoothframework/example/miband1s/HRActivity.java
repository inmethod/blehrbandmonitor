package inmethod.bluetoothframework.example.miband1s;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Message;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import ble.std.protocol.BTCommandsHandler.BTCommandsHandler;
import ble.std.protocol.HearRate.*;
import ble.std.protocol.battery.BTCommandsBatteryLevel;
import inmethod.android.bt.GlobalSetting;
import inmethod.android.bt.ScanRecord;
import inmethod.android.bt.BTInfo;
import inmethod.android.bt.DeviceConnection;
import inmethod.android.bt.command.BTCommands;
import inmethod.android.bt.handler.CommandCallbackHandler;
import inmethod.android.bt.handler.ConnectionCallbackHandler;
import inmethod.android.bt.handler.DiscoveryServiceCallbackHandler;
import inmethod.android.bt.interfaces.IChatService;
import inmethod.android.bt.interfaces.IDiscoveryService;
import inmethod.android.bt.le.LeChatService;
import inmethod.android.bt.le.LeDiscoveryService;
import inmethod.commons.util.HexAndStringConverter;

import android.os.Vibrator;

import java.text.*;

public class HRActivity extends AppCompatActivity implements DataApi.DataListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    //  Global Object
    private static final String TAG = "InMethod-BLE-HR";
    private IDiscoveryService aIBlueToothDiscoveryService = null;
    private DeviceConnection aBlueToothDeviceConnection = null;
    private IChatService aIBlueToothChatService = null;
    private AppCompatActivity activity = null;
    public static final String HeartRateUUID = "00002a37-0000-1000-8000-00805f9b34fb";
    private int MAX_HEART_RATE;
    public boolean bStopForSettings = false;
    private boolean bSkip = false;
    private int iPreviousHR = 0;
    private int iCurrentHR = 0;
    private int TOLERANCE;
    private int AGE;
    private String DEVICE_NAME;
    private TextView aTvHRLog1 = null;
    private TextView aTvHRLog2 = null;
    private TextView aTvBetteryStatus = null;
    private boolean bVibrated70 = false;
    private boolean bVibrated80 = false;
    private boolean bVibrated90 = false;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private PowerManager.WakeLock wakeLock = null;
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        aTvHRLog1 = (TextView) findViewById(R.id.HRLog1);
        aTvHRLog2 = (TextView) findViewById(R.id.HRLog2);
        aTvBetteryStatus = (TextView) findViewById(R.id.BetteryStatus);

        aTvHRLog1.setMovementMethod(ScrollingMovementMethod.getInstance());
        aTvHRLog2.setMovementMethod(ScrollingMovementMethod.getInstance());
        prepareDiscoveryService();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            this.setTurnScreenOn(true);
            KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
            keyguardManager.requestDismissKeyguard(this, null);
        } else {
            final Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("This app needs location access");
                builder.setMessage("When this permission is granted , please re-start this app");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                    }

                    ;
                });
                builder.show();
            }
        }

        int writeExternalStoragePermission = ContextCompat.checkSelfPermission(HRActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (writeExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
            // Request user to grant write external storage permission.
            ActivityCompat.requestPermissions(HRActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        }
        client = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

    }

    @Override
    protected void onResume() {
        super.onResume();
        client.connect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                // If request is cancelled, the result arrays are empty.
                Toast.makeText(activity, "location permission granted!", Toast.LENGTH_SHORT).show();
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        aIBlueToothDiscoveryService.startService();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(activity, "Please grant location permission", Toast.LENGTH_SHORT).show();
                }
                return;

        }
    }
    private void syncConfiguration() {
        if (client == null)
            return;
        Log.d(TAG, "syncConfiguration");
        final PutDataMapRequest putRequest = PutDataMapRequest.create("/HR_CONFIG");
        final DataMap map = putRequest.getDataMap();
        map.putInt("AGE", AGE);
        map.putInt("TOLERANCE", TOLERANCE);
        map.putString("DEVICE_NAME", DEVICE_NAME);
        Wearable.DataApi.putDataItem(client, putRequest.asPutDataRequest());
    }

   private void prepareDiscoveryService(){

       AGE = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(activity).getString("AGE", "35"));
       MAX_HEART_RATE = 220 - AGE;
       TOLERANCE = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(activity).getString("ToLerance", "25"));
       DEVICE_NAME = PreferenceManager.getDefaultSharedPreferences(activity).getString("DeviceName", "PS,350,810,850");
       bStopForSettings = false;
       boolean bSkip = false;
       iPreviousHR = 0;
       iCurrentHR = 0;


       // Use BLE discovery Service
       aIBlueToothDiscoveryService = LeDiscoveryService.getInstance();
       // Set context()
       aIBlueToothDiscoveryService.setContext(activity);
       ///  Set device name filter
       Vector<String> aBlueToothDeviceNameFilter = new Vector<String>();

       StringTokenizer aST = new StringTokenizer(DEVICE_NAME, ",");
       while (aST.hasMoreTokens()) {
           aBlueToothDeviceNameFilter.add(aST.nextToken());
       }

       aIBlueToothDiscoveryService.setBlueToothDeviceNameFilter(aBlueToothDeviceNameFilter);
       // Set CallBack handler
       aIBlueToothDiscoveryService.setCallBackHandler(new MyBlueToothDiscoveryServiceCallbackHandler());
   }
    @Override
    public void onStart() {
        super.onStart();
        bVibrated70 = false;
        bVibrated80 = false;
        bVibrated90 = false;

        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            try {
                aIBlueToothDiscoveryService.startService();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (aBlueToothDeviceConnection != null && aBlueToothDeviceConnection.isConnected()) {
            aBlueToothDeviceConnection.stop();
            aBlueToothDeviceConnection = null;
        }
        if (aIBlueToothDiscoveryService != null && aIBlueToothDiscoveryService.isRunning()) {
            aIBlueToothDiscoveryService.cancelDiscovery();
            aIBlueToothDiscoveryService.stopService();
            aIBlueToothDiscoveryService = null;
        }
        if (wakeLock != null && wakeLock.isHeld()) wakeLock.release();
        Toast.makeText(activity, "Device disconnected!", Toast.LENGTH_SHORT).show();
        this.finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (aBlueToothDeviceConnection != null && aBlueToothDeviceConnection.isConnected()) {
            aBlueToothDeviceConnection.stop();
            aBlueToothDeviceConnection = null;
        }
        if (aIBlueToothDiscoveryService != null && aIBlueToothDiscoveryService.isRunning()) {
            aIBlueToothDiscoveryService.cancelDiscovery();
            aIBlueToothDiscoveryService.stopService();
            aIBlueToothDiscoveryService = null;
        }
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(Write.getInstance(activity).getFileName());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
        Write.getInstance(activity).close();
    }

    @Override
    public void onStop() {
        super.onStop();


        client.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "GoogleApiClient onConnected: ");
        Wearable.DataApi.addListener(client, this);
        syncConfiguration();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "GoogleApiClient onConnectionSuspended: ");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Wearable.DataApi.removeListener(client, this);
        client.disconnect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        Log.d(TAG, "GoogleApiClient onDataChanged: ");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "GoogleApiClient onConnectionFailed: ");

    }


    public class MyBlueToothDiscoveryServiceCallbackHandler extends DiscoveryServiceCallbackHandler {


        @Override
        public void StartServiceStatus(boolean bStatus, int iCode) {
            if (bStatus && iCode == DiscoveryServiceCallbackHandler.START_SERVICE_SUCCESS) {
                if (aIBlueToothDiscoveryService.isRunning())
                    aIBlueToothDiscoveryService.doDiscovery();
            } else if (!bStatus && iCode == DiscoveryServiceCallbackHandler.START_SERVICE_BLUETOOTH_NOT_ENABLE) {
                Toast.makeText(activity, "SERVICE_BLUETOOTH_NOT_ENABLE ", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void DeviceDiscoveryStatus(boolean bStatus, BTInfo aBTInfo) {
            if (bStatus) {
                // enable notification
                ArrayList<String> aNotifyUUIDs = new ArrayList<String>();
                aNotifyUUIDs.add(BTCommandsBatteryLevel.BATTERY_LEVEL_UUID);
                aNotifyUUIDs.add(HeartRateUUID);
                aIBlueToothChatService = new LeChatService(BluetoothAdapter.getDefaultAdapter(), activity, aNotifyUUIDs);
                // create new connection object and set up call back handler
                aBlueToothDeviceConnection = new DeviceConnection(aBTInfo, activity,
                        aIBlueToothChatService, new MyBlueToothConnectionCallbackHandler());
                // Connect to device
                aBlueToothDeviceConnection.connect();
                Toast.makeText(activity, "device=" + aBTInfo.getDeviceName() + ",address=" + aBTInfo.getDeviceAddress(), Toast.LENGTH_SHORT).show();
            } else if (!bStatus) {
                Log.d(TAG, "Device not found!");
                try {
                    aIBlueToothDiscoveryService.doDiscovery();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class MyBlueToothConnectionCallbackHandler extends ConnectionCallbackHandler {


        @Override
        public void DeviceConnectionStatus(boolean bStatus, BTInfo aBTInfo) {
            if (bStatus) {

            } else {
                if (aBlueToothDeviceConnection != null && !bStopForSettings)
                    aBlueToothDeviceConnection.connect();
            }
        }


        @Override
        public void NotificationStatus(boolean bStatus, BTInfo aBTInfo, String sReaderUUID) {
            Log.d(TAG, "status=" + bStatus + ",notification uuid = " + sReaderUUID);
            if (bStatus) {
                if (sReaderUUID != null && sReaderUUID.equalsIgnoreCase(BTCommandsBatteryLevel.BATTERY_LEVEL_UUID)){
                    sendBatteryBTCommands();
                }
                else if (sReaderUUID != null && sReaderUUID.equalsIgnoreCase(BTCommandsHRMeasurement.HEART_RATE_MEASUREMENT_UUID)) {
                    sendBTCommands();
                }
            }
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.scan_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ic_menu_setting:
                if (aBlueToothDeviceConnection != null && aBlueToothDeviceConnection.isConnected())
                    aBlueToothDeviceConnection.stop();
                aIBlueToothDiscoveryService.stopService();
                bStopForSettings = true;
                Intent Intent = new Intent(HRActivity.this, ActPrefs.class);
                startActivity(Intent);
                break;
        }
        return true;
    }


    private void sendBatteryBTCommands() {

        BTCommands aBatteryLevel = new BTCommandsBatteryLevel();
        aBatteryLevel.setCallBackHandler(new BTCommandsHandler() {
            public void BTCommandsBatteryLevelSuccess(BTInfo aBTInfo, final int iBetteryLevel) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        aTvBetteryStatus.setText("HeartRate and Time , Battery = " + iBetteryLevel);
                    }
                });
            }

            public void BTCommandsBatteryLevelTimeout(BTInfo aBTInfo) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        aTvBetteryStatus.setText("HeartRate and Time");
                    }
                });
            }
        });
        aBlueToothDeviceConnection.sendBTCommands(aBatteryLevel);
    }

    private void sendBTCommands() {
        BTCommands aBTCommands = new BTCommandsHRMeasurement(Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(activity).getString("AGE", "35")));
        aBTCommands.setTimeout(0); // no timeout
        aBTCommands.setCallBackHandler(
                new BTCommandsHandler() {
                    @Override
                    public void BTCommandsHeartRateMeasurementSuccess(HeartRateValue aHeartRateValue, BTInfo aInfo) {

                        if (aHeartRateValue == null) return;
                        iCurrentHR = aHeartRateValue.getHeartRate();
                        if (iCurrentHR > 220 || iCurrentHR < 30) return;

                        if (iPreviousHR == 0) iPreviousHR = iCurrentHR;
                        Log.d(TAG, "Current HeartRate=" + iCurrentHR + ",Previous =" + iPreviousHR);
                        if ((iCurrentHR > (iPreviousHR + TOLERANCE)) || (iCurrentHR < (iPreviousHR - TOLERANCE))) {
                            if (bSkip) {
                                bSkip = false;
                                iPreviousHR = iCurrentHR;
                            } else bSkip = true;
                        } else {
                            iPreviousHR = iCurrentHR;
                            bSkip = false;
                        }
                        if (!bSkip) {
                            BTLog(iCurrentHR);
                            if (iCurrentHR >= ((int) MAX_HEART_RATE * 0.7)) {
                                if (iCurrentHR >= ((int) MAX_HEART_RATE * 0.9)) {
                                    if (!bVibrated90) {
                                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                        v.vibrate(2000);
                                        bVibrated90 = true;
                                    }
                                    bVibrated80 = false;
                                    bVibrated70 = false;
                                } else if (iCurrentHR >= ((int) MAX_HEART_RATE * 0.8)) {
                                    if (!bVibrated80) {
                                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                        v.vibrate(1000);
                                        bVibrated80 = true;
                                    }

                                    bVibrated90 = false;
                                    bVibrated70 = false;
                                } else if (iCurrentHR >= ((int) MAX_HEART_RATE * 0.7)) {
                                    if (!bVibrated70) {
                                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                        v.vibrate(400);
                                        bVibrated70 = true;
                                    }
                                    bVibrated90 = false;
                                    bVibrated80 = false;
                                }
                            } else {
                            }
                        } else {
                            Log.d(TAG, "Heart Rate Not Reasonable current = " + iCurrentHR + ",Previous = " + iPreviousHR);
                        }

                    }

                    @Override
                    public void BTCommandsHeartRateMeasurementTimeout(BTInfo aInfo) {
                        Log.d(TAG, "command Timeout");
                    }
                }
        );
        aBlueToothDeviceConnection.sendBTCommands(aBTCommands);

    }

    private void BTLog(final int iHR) {

        runOnUiThread(new Runnable() {
                          public void run() {
                             SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss ");
                              SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ") ;
                              Date aD = Calendar.getInstance().getTime();
                              String sDate = format.format(aD);
                              String sDate2 = format2.format(aD);
                              DecimalFormat df = new DecimalFormat("###");
                              String sHR = df.format(iHR);
                              sHR = String.format("%3s", sHR);
                              writeaTvHRLog1(sHR+"\n");
                              writeaTvHRLog2(sDate+"\n");
                              Write.getInstance(activity).writeMessage(sDate2+","+sHR+"\r\n");
                              /*
                              if (!aTvHRLog1.getText().toString().trim().equals("")) {
                                  aTvHRLog1.setText(sHR + "\n" + aTvHRLog1.getText());
                                  aTvHRLog2.setText(sDate + "\n" + aTvHRLog2.getText());
                              } else {
                                  aTvHRLog1.setText(sHR);
                                  aTvHRLog2.setText(sDate);
                              }*/
                          }
                      }
        );

    }
    public void writeaTvHRLog1(String data) {
        aTvHRLog1.append(data);
        // Erase excessive lines
        int excessLineNumber = aTvHRLog1.getLineCount() - 11;
        if (excessLineNumber > 0) {
            int eolIndex = -1;
            CharSequence charSequence = aTvHRLog1.getText();
            for(int i=0; i<excessLineNumber; i++) {
                do {
                    eolIndex++;
                } while(eolIndex < charSequence.length() && charSequence.charAt(eolIndex) != '\n');
            }
            if (eolIndex < charSequence.length()) {
                aTvHRLog1.getEditableText().delete(0, eolIndex+1);
            }
            else {
                aTvHRLog1.setText("");
            }
        }
    }
    public void writeaTvHRLog2(String data) {
        aTvHRLog2.append(data);
        // Erase excessive lines
        int excessLineNumber = aTvHRLog2.getLineCount() - 11;
        if (excessLineNumber > 0) {
            int eolIndex = -1;
            CharSequence charSequence = aTvHRLog2.getText();
            for(int i=0; i<excessLineNumber; i++) {
                do {
                    eolIndex++;
                } while(eolIndex < charSequence.length() && charSequence.charAt(eolIndex) != '\n');
            }
            if (eolIndex < charSequence.length()) {
                aTvHRLog2.getEditableText().delete(0, eolIndex+1);
            }
            else {
                aTvHRLog2.setText("");
            }
        }
    }

}


 class Write {
    private String sFileName = "ICP";
    private FileOutputStream fop = null;
    private File sdcard;
    private File file;
    private static Activity activity = null;
    private static Write aWrite = null;

    private Write(String ss) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd") ;
        Date aD = Calendar.getInstance().getTime();
        String sDate = format.format(aD);
        if (ss != null) {
            this.sFileName =  ss + "_" +sDate+ "-Log.csv";
        }
        else return;
        try {
            new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+ File.separator+ PreferenceManager.getDefaultSharedPreferences(activity).getString("DownloadSubDir", "InMethod")).mkdirs();
            this.file = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+ File.separator+ PreferenceManager.getDefaultSharedPreferences(activity).getString("DownloadSubDir", "InMethod"), this.sFileName);
            if (!this.file.exists()) {
                this.file.createNewFile();
            }

            this.fop = new FileOutputStream(this.file,true);
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    public static Write getInstance(Activity act,String sFileName) {
        aWrite = new Write(sFileName);
        activity = act;
        return aWrite;
    }

    public static Write getInstance(Activity act) {
        activity = act;
        if (aWrite == null) {
            aWrite = new Write("HR");
        }

        return aWrite;
    }

    public String getFileName() {
        return this.file != null ? this.file.getAbsolutePath() : this.sFileName;
    }

    public void close() {
        try {
            if (this.fop != null) {
                this.fop.close();
            }

            aWrite = null;
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    public void writeMessage(String message) {
        String content = message;

        try {
            byte[] contentInBytes = content.getBytes();
            this.fop.write(contentInBytes);
            this.fop.flush();
        } catch (IOException var4) {
            var4.printStackTrace();
        }

    }
}