package inmethod.bluetoothframework.example.miband1s;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class ActPrefs extends PreferenceActivity{
	 
  @Override
public void onCreate(Bundle savedInstanceState) {
   super.onCreate(savedInstanceState);

      addPreferencesFromResource(R.xml.prefs);
   //getFragmentManager().beginTransaction().replace(android.R.id.content,    new PrefsFrag()).commit();
 }
}